# Assignment

Implement a component which is responsible for selling tickets for events. Events can be anything from a small seminar with tens of visitors up to concerts with multiple thousands. A ticket would be for example a printed ticket or an e-ticket. The component has to provide an API which is assumed to be consumed by other distributed components.

Concentrate on the implementation of the ticketing logic, you can assume that user handling, event creation and payment etc is already present.

## Requirements


### Functional requirements

Important use cases which should be at least partly implemented:
- Creation of a new ticket
- Query tickets by search criteria (e.g. get all tickets by user or get all tickets for a certain event)
- Updating ticket details (eg when the start time of the event was changed)
- Making a ticket invalid (eg when there was a major change in the event)

### Non-functional requirements

- Implement the component in Scala
- Write automated tests (unit tests, service tests) as u would typically also do for your production code
- You are free to choose the API technology u want (eg HTTP, gRPC, Websocket) but note that u have to provide good API documentation
- You are free to choose any framework or library u want (eg akka, lagom, play)
- you dont need to provide real persistence, its fine to mock the database
- you dont need to deploy the component in any form, its fine to just start it from the command line
- write clean, modular and well structured idiomatic scala code
- take great care about the API design
- treat the API and code as it would go into production, this means it should be easy to analyze/debug, have robust error handling and be reasonably efficient
- take care about concurrent user interactions (eg: what happens if multiple users want to buy tickets at the same time but there is only a limited number of tickets available)
- assume that the component is deployed in the cloud (eg follow twelve factor app -> https://12factor.net/de/) 
