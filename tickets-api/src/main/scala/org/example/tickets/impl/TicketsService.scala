package org.example.tickets.impl

import akka.stream.scaladsl.Source
import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.transport.Method._
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}

trait TicketsService extends Service {

  def createEvent(): ServiceCall[Event, Done]

  def updateEvent(eventId: String): ServiceCall[Event, Done]

  def reserveTicket(eventId: String): ServiceCall[TicketReservation, Done]

  def getReservations(eventId: String, userId: String): ServiceCall[NotUsed, List[TicketDetails]]

  def purchaseTickets(eventId: String, userId: String): ServiceCall[NotUsed, Done]

  def getPurchased(eventId: String, userId: String): ServiceCall[NotUsed, List[TicketDetails]]

  def getAllSold(eventId: String): ServiceCall[NotUsed, List[TicketDetails]]

  def getTicketsInStatus(userId: String, status: String): ServiceCall[NotUsed, List[TicketDetails]]

  override def descriptor: Descriptor = {
    import Service._
    named("tickets")
      .withCalls(
        restCall(POST, "/api/events", createEvent _),
        restCall(POST, "/api/events/:eventId", updateEvent _),
        restCall(POST, "/api/events/:eventId/tickets", reserveTicket _),
        restCall(GET, "/api/events/:eventId/reservations/:userId", getReservations _),
        restCall(PUT, "/api/events/:eventId/tickets/:userId", purchaseTickets _),
        restCall(GET, "/api/events/:eventId/tickets/:userId", getPurchased _),
        restCall(GET, "/api/events/:eventId/tickets", getAllSold _),
        restCall(GET, "/api/tickets/:userId/:status", getTicketsInStatus _)
      )
      .withAutoAcl(true)
  }
}

case class Event(eventId: String, maxVisitors: Int, eventDate: String, address: String)

object Event {
  implicit val format: Format[Event] = Json.format
}

case class TicketReservation(userId: String)

object TicketReservation {
  implicit val format: Format[TicketReservation] = Json.format
}

case class TicketDetails(ticketId: Option[String],
                         eventId: String,
                         reservedUntil: Option[Long],
                         price: Option[BigDecimal])

object TicketDetails {
  implicit val format: Format[TicketDetails] = Json.format[TicketDetails]
}
