package org.example.tickets.impl

import akka.Done
import com.lightbend.lagom.scaladsl.api.transport.BadRequest
import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}

class TicketsServiceSpec extends AsyncWordSpec with Matchers with BeforeAndAfterAll {

  private val server = ServiceTest.startServer(
    ServiceTest.defaultSetup
      .withCassandra()
  ) { ctx =>
    new TicketsApplication(ctx) with LocalServiceLocator
  }

  val client: TicketsService = server.serviceClient.implement[TicketsService]

  override protected def afterAll(): Unit = server.stop()

  "tickets service" should {

    "allow creating events" in {
      client.createEvent().invoke(Event("1", 10, "", "")).map { answer =>
        answer should be(Done)
      }
    }

    "disallow creating events with non-positive visitors" in {
      recoverToExceptionIf[BadRequest] {
        client.createEvent().invoke(Event("2", 0, "", ""))
      } map { ex =>
        ex should have message "Number of visitors cannot be zero or negative"
      }
    }

    "allow getting 2 purchased tickets by user" in {
      for {
        createAnswer <- client.createEvent().invoke(Event("event-3", 10, "Unknown date", "Unknown palce"))
        _ = createAnswer should be(Done)
        reserveAnswer1 <- client.reserveTicket("event-3").invoke(TicketReservation("user-1"))
        _ = reserveAnswer1 should be(Done)
        reserveAnswer2 <- client.reserveTicket("event-3").invoke(TicketReservation("user-1"))
        _ = reserveAnswer2 should be(Done)
        purchaseAnswer <- client.purchaseTickets("event-3", "user-1").invoke()
        _ = purchaseAnswer should be(Done)
        answer <- client.getPurchased("event-3", "user-1").invoke()
      } yield {
        answer should have size 2
        for (ticket <- answer) {
          ticket.ticketId should not be None
          ticket.eventId should be("event-3")
          ticket.reservedUntil should be(None)
        }
        succeed
      }
    }

    "allow updating event details" in {
      for {
        createAnswer <- client.createEvent().invoke(Event("event-4", 10, "Unknown date", "Unknown palce"))
        _ = createAnswer should be(Done)
        updateAnswer <- client.updateEvent("event-4").invoke(Event("event-4", 5, "10-10-2020", "In the middle of nowhere"))
        _ = updateAnswer should be (Done)
      } yield {
        succeed
      }
    }
  }
}
