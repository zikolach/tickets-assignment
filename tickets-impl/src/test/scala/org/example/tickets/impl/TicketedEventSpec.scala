package org.example.tickets.impl

import java.util.UUID

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.persistence.typed.PersistenceId
import org.joda.time.DateTime
import org.scalatest.{Matchers, WordSpecLike}

class TicketedEventSpec extends ScalaTestWithActorTestKit(
  s"""
      akka.persistence.journal.plugin = "akka.persistence.journal.inmem"
      akka.persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
      akka.persistence.snapshot-store.local.dir = "target/snapshot-${UUID.randomUUID().toString}"
    """) with WordSpecLike with Matchers {

  import TicketedEvent._

  "ticketed event" should {

    "allow creating event" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId("fake-type-hint", "fake-first-id")))
      ref ! CreateEvent("123", 10, "", "", probe.ref)
      probe.expectMessage(Confirmed)
    }

    "allow requesting reserved tickets" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId("fake-type-hint", "fake-second-id")))
      ref ! CreateEvent("1234", 10, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! ReserveTicket("user1", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! GetReservations("user1", probe.ref)
      val reserved = probe.expectMessageType[UserReservations]
      reserved.tickets should have size 1
    }

    "allow reserve tickets until max reached" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId(TicketedEvent.typeKey.name, "event-3")))
      ref ! CreateEvent("event-3", 1, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! ReserveTicket("user-1", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! ReserveTicket("user-1", probe.ref)
      probe.expectMessage(Rejected("No tickets available"))
    }

    "allow to buy tickets" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId(TicketedEvent.typeKey.name, "event-4")))
      ref ! CreateEvent("event-4", 1, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! ReserveTicket("user-1", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! BuyTickets("user-1", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! GetReservations("user1", probe.ref)
      val reserved = probe.expectMessageType[UserReservations]
      reserved.tickets should have size 0
      ref ! GetTickets("user-1", probe.ref)
      val purchased = probe.expectMessageType[UserTickets]
      purchased.tickets should have size 1
    }

    "allow getting all sold tickets" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId(TicketedEvent.typeKey.name, "event-5")))
      ref ! CreateEvent("event-5", 4, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      val users = List("user-1", "user-2", "user-1")
      for (user <- users) {
        ref ! ReserveTicket(user, probe.ref)
        ref ! BuyTickets(user, probe.ref)
        probe.receiveMessages(2)
      }
      ref ! GetSoldTickets(probe.ref)
      val purchased = probe.expectMessageType[UserTickets]
      purchased.tickets should have size users.size
      purchased.tickets.map(_.userId) should ===(users.toSet)
    }

    "allow reserve outdated reservations" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId(TicketedEvent.typeKey.name, "event-6")))
      ref ! CreateEvent("event-6", 1, "", "", probe.ref, 0)
      probe.expectMessage(Confirmed)
      ref ! ReserveTicket("user-1", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! GetReservations("user-1", probe.ref)
      probe.expectMessageType[UserReservations].tickets should have size 1
      Thread.sleep(5)
      ref ! ReserveTicket("user-2", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! GetReservations("user-2", probe.ref)
      probe.expectMessageType[UserReservations].tickets should have size 1
    }

    "disallow creating event twice" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId("fake-type-hint", "event-7")))
      ref ! CreateEvent("event-7", 10, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      ref ! CreateEvent("event-7", 10, "", "", probe.ref)
      probe.expectMessage(Rejected("Event event-7 is already created"))
    }

    "allow updating events" in {
      val probe = createTestProbe[CommandReply]()
      val ref = spawn(TicketedEvent(PersistenceId("fake-type-hint", "event-8")))
      ref ! CreateEvent("event-8", 10, "", "", probe.ref)
      probe.expectMessage(Confirmed)
      val eventDate = DateTime.parse("2020-01-01").toString()
      ref ! UpdateEvent(20, eventDate, "Somewhere", probe.ref)
      probe.expectMessage(Confirmed)
    }
  }
}
