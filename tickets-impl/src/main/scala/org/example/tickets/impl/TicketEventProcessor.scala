package org.example.tickets.impl

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}
import org.example.tickets.impl.TicketedEvent.{EventCreated, EventUpdated, ReservationConfirmed, TicketReservationReleased, TicketReserved}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future, Promise}

class TicketEventProcessor(session: CassandraSession, readSide: CassandraReadSide)(implicit ec: ExecutionContext)
  extends ReadSideProcessor[TicketedEvent.Event] {

  private final val log: Logger = LoggerFactory.getLogger(classOf[TicketEventProcessor])

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[TicketedEvent.Event] = {
    val builder = readSide.builder[TicketedEvent.Event]("ticketdetailsoffset")
    builder.setGlobalPrepare(() => createTable())
    builder.setPrepare(prepare)
    builder.setEventHandler[EventCreated](handlerEventCreated)
    builder.setEventHandler[EventUpdated](handlerEventUpdated)
    builder.setEventHandler[TicketReserved](handlerTicketReserved)
    builder.setEventHandler[ReservationConfirmed](handlerReservationConfirmed)
    builder.setEventHandler[TicketReservationReleased](handlerTicketReservationReleased)
    builder.build()
  }

  private val eventCreatePromise                     = Promise[PreparedStatement]
  private def eventCreate: Future[PreparedStatement] = eventCreatePromise.future
  private val eventUpdatePromise                     = Promise[PreparedStatement]
  private def eventUpdate: Future[PreparedStatement] = eventUpdatePromise.future
  private val ticketCreatePromise                     = Promise[PreparedStatement]
  private def ticketCreate: Future[PreparedStatement] = ticketCreatePromise.future
  private val ticketUpdatePromise                     = Promise[PreparedStatement]
  private def ticketUpdate: Future[PreparedStatement] = ticketUpdatePromise.future
  private val ticketDeletePromise                     = Promise[PreparedStatement]
  private def ticketDelete: Future[PreparedStatement] = ticketDeletePromise.future

  def prepare(eventTag: AggregateEventTag[TicketedEvent.Event]): Future[Done] = {
    for {
      _ <- eventCreatePromise.completeWith(session.prepare("INSERT INTO event_details (id, address, event_date) VALUES (?, ?, ?)")).future
      _ <- eventUpdatePromise.completeWith(session.prepare("UPDATE event_details SET address = ?, event_date = ? WHERE id = ?")).future
      _ <- ticketCreatePromise.completeWith(session.prepare("INSERT INTO ticket_details (id, event_id, user_id, status) VALUES (?, ?, ?, ?)")).future
      _ <- ticketUpdatePromise.completeWith(session.prepare("UPDATE ticket_details SET status = ? WHERE user_id = ? AND id = ?")).future
      _ <- ticketDeletePromise.completeWith(session.prepare("DELETE FROM ticket_details WHERE user_id = ? AND id = ?")).future
    } yield Done
  }

  private def handlerEventCreated(eventElement: EventStreamElement[EventCreated]): Future[List[BoundStatement]] = {
    logHnadledEvent(eventElement)
    eventCreate.map { ps =>
      val bind = ps.bind()
      bind.setString("id", eventElement.event.eventId)
      bind.setString("address", eventElement.event.address)
      bind.setString("event_date", eventElement.event.eventDate)
      List(bind)
    }
  }

  private def logHnadledEvent(eventElement: EventStreamElement[_]) = {
    log.info(s"Processing event [${eventElement.event}] for entity [${eventElement.entityId}] and offset [${eventElement.offset}]")
  }

  private def handlerEventUpdated(eventElement: EventStreamElement[EventUpdated]): Future[List[BoundStatement]] = {
    logHnadledEvent(eventElement)
    eventUpdate.map { ps =>
      val bind = ps.bind()
      bind.setString("address", eventElement.event.address)
      bind.setString("event_date", eventElement.event.eventDate)
      bind.setString("id", eventElement.entityId)
      List(bind)
    }
  }

  private def handlerTicketReserved(eventElement: EventStreamElement[TicketReserved]): Future[List[BoundStatement]] = {
    logHnadledEvent(eventElement)
    ticketCreate.map { ps =>
      val bind = ps.bind()
      bind.setString("id", eventElement.event.ticketId)
      bind.setString("event_id", eventElement.entityId)
      bind.setString("user_id", eventElement.event.userId)
      bind.setString("status", "RESERVED")
      List(bind)
    }
  }

  private def handlerTicketReservationReleased(eventElement: EventStreamElement[TicketReservationReleased]): Future[List[BoundStatement]] = {
    logHnadledEvent(eventElement)
    ticketDelete.map { ps =>
      val bind = ps.bind()
      bind.setString("user_id", eventElement.event.ticketId)
      bind.setString("id", eventElement.event.userId)
      List(bind)
    }
  }

  private def handlerReservationConfirmed(eventElement: EventStreamElement[ReservationConfirmed]): Future[List[BoundStatement]] = {
    logHnadledEvent(eventElement)
    ticketUpdate.map { ps =>
      eventElement.event.tickets.toList.map { ticketId =>
        val bind = ps.bind()
        bind.setString("status", "PURCHASED")
        bind.setString("user_id", eventElement.event.userId)
        bind.setString("id", ticketId)
        bind
      }
    }
  }

  private def createTable(): Future[Done] = {
    for {
      _ <- session.executeCreateTable(
        """
          |CREATE TABLE IF NOT EXISTS ticket_details (
          |  id TEXT,
          |  event_id TEXT,
          |  user_id TEXT,
          |  status TEXT,
          |  PRIMARY KEY ((user_id), id)
          |)
          |""".stripMargin)
      _ <- session.executeCreateTable(
        """
          |CREATE TABLE IF NOT EXISTS event_details (
          |  id TEXT,
          |  address TEXT,
          |  event_date TEXT,
          |  PRIMARY KEY (id)
          |)
          |""".stripMargin
      )
      _ <- session.executeCreateTable(
        """
          |CREATE INDEX ticket_details_status ON ticket_details (status)
          |""".stripMargin
      )
    } yield Done
  }

  override def aggregateTags: Set[AggregateEventTag[TicketedEvent.Event]] =
    TicketedEvent.Event.Tag.allTags
}
