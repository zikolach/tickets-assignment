package org.example.tickets.impl

import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef}
import akka.util.Timeout
import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.BadRequest
import com.lightbend.lagom.scaladsl.persistence.ReadSide
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import org.example.tickets.impl
import org.example.tickets.impl.TicketedEvent._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class TicketsServiceImpl(clusterSharding: ClusterSharding,
                         cassandraSession: CassandraSession,
                         cassandraReadSide: CassandraReadSide,
                         readSide: ReadSide)
                        (implicit val ec: ExecutionContext)
  extends TicketsService {

  implicit val timeout = Timeout(5.seconds)

  readSide.register[TicketedEvent.Event](new TicketEventProcessor(cassandraSession, cassandraReadSide))

  private def entityRef(id: String): EntityRef[Command[_]] = clusterSharding.entityRefFor(TicketedEvent.typeKey, id)

  override def createEvent() = ServiceCall {
    event => {
      val ref = entityRef(event.eventId)

      ref.ask[CommandReply](
        replyTo => CreateEvent(event.eventId, event.maxVisitors, event.eventDate, event.address, replyTo)
      ).map {
        case Confirmed => Done
        case Rejected(reason) => throw BadRequest(reason)
        case _ => throw BadRequest("Can't create an event.")
      }
    }
  }

  override def reserveTicket(eventId: String) = ServiceCall {
    reservation => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        replyTo => ReserveTicket(reservation.userId, replyTo)
      ).map {
        case Confirmed => Done
        case _ => throw BadRequest("Can't reserve a ticket.")
      }
    }
  }

  override def getReservations(eventId: String, userId: String) = ServiceCall {
    _ => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        replyTo => GetReservations(userId, replyTo)
      ).map {
        case UserReservations(tickets) => tickets.map(t => TicketDetails(None, eventId, Some(t.validUntil), Some(t.price))).toList
        case _ => throw BadRequest("Can't get ticket reservations.")
      }
    }
  }

  override def getPurchased(eventId: String, userId: String): ServiceCall[NotUsed, List[TicketDetails]] = ServiceCall {
    _ => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        replyTo => GetTickets(userId, replyTo)
      ).map {
        case UserTickets(tickets) => tickets.map(t => TicketDetails(Some(t.ticketId), eventId, None, Some(t.price))).toList
        case _ => throw BadRequest("Can't get purchased tickets.")
      }
    }
  }

  override def purchaseTickets(eventId: String, userId: String): ServiceCall[NotUsed, Done] = ServiceCall {
    _ => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        // TODO: check number of tickets (reserved vs expected)
        replyTo => BuyTickets(userId, replyTo)
      ).map {
        case Confirmed => Done
        case _ => throw BadRequest("Can't purchase tickets.")
      }
    }
  }

  override def updateEvent(eventId: String): ServiceCall[impl.Event, Done] = ServiceCall {
    event => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        replyTo => UpdateEvent(event.maxVisitors, event.eventDate, event.address, replyTo)
      ).map {
        case Confirmed => Done
        case Rejected(reason) => throw BadRequest(reason)
        case _ => throw BadRequest("Can't create an event.")
      }
    }
  }

  override def getAllSold(eventId: String): ServiceCall[NotUsed, List[TicketDetails]] = ServiceCall {
    _ => {
      val ref = entityRef(eventId)

      ref.ask[CommandReply](
        replyTo => GetSoldTickets(replyTo)
      ).map {
        case UserTickets(tickets) => tickets.map(t => TicketDetails(Some(t.ticketId), eventId, None, Some(t.price))).toList
        case _ => throw BadRequest("Can't get sold tickets.")
      }
    }
  }

  override def getTicketsInStatus(userId: String, status: String) = ServiceCall {
    _ =>
      cassandraSession
        .selectAll(
          """
            |SELECT id, event_id, user_id
            |FROM ticket_details
            |WHERE user_id = ? AND status = ?
            """.stripMargin,
          userId, status
        )
        .map(_.map(row => TicketDetails(
          Some(row.getString("id")),
          row.getString("event_id"),
          None,
          None
        )).toList)
  }
}
