package org.example.tickets.impl

import java.util.UUID

import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl.{EntityContext, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventShards, AggregateEventTag, AkkaTaggerAdapter}
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import org.example.tickets.impl.TicketedEvent._
import play.api.libs.json._

import scala.concurrent.duration._
import scala.util.Random

object TicketedEvent {
  val typeKey = EntityTypeKey[Command[_]]("TicketedEvent")

  // State
  sealed trait Ticket {
    def price: BigDecimal
  }

  object Ticket {
    val MAX_RESERVATION_TIME = 5.minutes
  }

  case class ReservedTicket(ticketId: String, userId: String, price: BigDecimal, validUntil: Long) extends Ticket

  case class SoldTicket(ticketId: String, userId: String, price: BigDecimal) extends Ticket

  // Command
  sealed trait CommandSerializable

  sealed trait Command[Reply <: CommandReply] extends CommandSerializable {
    def replyTo: ActorRef[Reply]
  }

  case class CreateEvent(eventId: String,
                         maxVisitors: Int,
                         eventDate: String,
                         address: String,
                         replyTo: ActorRef[CommandReply],
                         maxReservationTime: Long = Ticket.MAX_RESERVATION_TIME.toMillis) extends Command[CommandReply]

  case class UpdateEvent(maxVisitors: Int,
                         eventDate: String,
                         address: String,
                         replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  case class ReserveTicket(userId: String, replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  case class GetReservations(userId: String, replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  case class BuyTickets(userId: String, replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  case class GetTickets(userId: String, replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  case class GetSoldTickets(replyTo: ActorRef[CommandReply]) extends Command[CommandReply]

  // Reply
  sealed trait CommandReply

  case object Confirmed extends CommandReply

  case class UserReservations(tickets: Set[ReservedTicket]) extends CommandReply

  case class UserTickets(tickets: Set[SoldTicket]) extends CommandReply

  case class Rejected(reason: String) extends CommandReply

  // Event
  sealed trait Event extends AggregateEvent[Event] {
    def aggregateTag: AggregateEventShards[Event] = Event.Tag
  }

  object Event {
    val Tag: AggregateEventShards[Event] = AggregateEventTag.sharded[Event](10)
  }

  case class EventCreated(eventId: String, maxVisitors: Int, eventDate: String, address: String, maxReservationTime: Long) extends Event

  object EventCreated {
    implicit val format: Format[EventCreated] = Json.format
  }

  case class EventUpdated(maxVisitors: Int, eventDate: String, address: String) extends Event

  object EventUpdated {
    implicit val format: Format[EventUpdated] = Json.format
  }

  case class TicketReserved(userId: String, ticketId: String, at: Long) extends Event

  object TicketReserved {
    implicit val format: Format[TicketReserved] = Json.format
  }

  case class TicketReservationReleased(ticketId: String, userId: String, reason: String) extends Event

  object TicketReservationReleased {
    implicit val format: Format[TicketReservationReleased] = Json.format
  }

  case class ReservationConfirmed(userId: String, tickets: Seq[String]) extends Event

  object ReservationConfirmed {
    implicit val format: Format[ReservationConfirmed] = Json.format
  }

  def apply(entityContext: EntityContext[Command[_]]): Behavior[Command[_]] = {
    val persistenceId: PersistenceId = PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
    apply(persistenceId).withTagger(AkkaTaggerAdapter.fromLagom(entityContext, Event.Tag))
  }

  def apply(persistenceId: PersistenceId): EventSourcedBehavior[Command[_], Event, Option[TicketedEvent]] = {
    EventSourcedBehavior.withEnforcedReplies[Command[_], Event, Option[TicketedEvent]](
      persistenceId,
      None,
      (state, command) => state match {
        case None => onFirstCommand(command)
        case Some(ticketedEvent) => ticketedEvent.applyCommand(command)
      },
      (state, event) => state match {
        case None => onFirstEvent(event)
        case Some(ticketedEvent) => Some(ticketedEvent.applyEvent(event))
      }
    )
  }

  private def onFirstEvent(event: Event): Option[TicketedEvent] = {
    event match {
      case EventCreated(eventId, maxVisitors, eventDate, address, maxReservationTime) =>
        Some(TicketedEvent(
          eventId, maxVisitors,
          eventDate, address,
          maxReservationTime = maxReservationTime
        ))
      case _ => throw new IllegalStateException(s"Unexpected event [$event] in state [NonExistingEvent]")
    }
  }

  private def onFirstCommand(command: Command[_]): ReplyEffect[Event, Option[TicketedEvent]] = {
    command match {
      case CreateEvent(_, maxVisitors, _, _, replyTo, _) if maxVisitors <= 0 =>
        Effect.reply(replyTo)(Rejected("Number of visitors cannot be zero or negative"))
      case CreateEvent(eventId, maxVisitors, eventDate, address, replyTo, maxReservationTime) =>
        Effect
          .persist(EventCreated(eventId, maxVisitors, eventDate, address, maxReservationTime))
          .thenReply(replyTo)(_ => Confirmed)
      case _ =>
        Effect.reply(command.replyTo.asInstanceOf[ActorRef[CommandReply]])(Rejected("Non-existing event"))
      //        throw new IllegalStateException(s"Unexpected command [$command] in state [NonExistingEvent]")
    }
  }
}

final case class TicketedEvent(eventId: String,
                               maxVisitors: Int,
                               eventDate: String,
                               address: String,
                               tickets: Set[Ticket] = Set.empty,
                               maxReservationTime: Long = Ticket.MAX_RESERVATION_TIME.toMillis) {
  require(maxVisitors >= tickets.size)
  require(maxReservationTime >= 0)

  def releaseOutdatedReservations(atMaxValid: Long = System.currentTimeMillis()): Set[Event] = {
    tickets.collect {
      case ticket: ReservedTicket if ticket.validUntil < atMaxValid =>
        TicketReservationReleased(ticket.ticketId, ticket.userId, "Reservation outdated")
    }
  }

  def applyCommand(cmd: Command[_]): ReplyEffect[Event, Option[TicketedEvent]] = cmd match {
    case ReserveTicket(userId, replyTo) =>
      val outdatedReservations: Seq[Event] = releaseOutdatedReservations().toSeq
      if (tickets.size - outdatedReservations.size < maxVisitors)
        Effect
          .persist(outdatedReservations :+ TicketReserved(userId, UUID.randomUUID().toString, System.currentTimeMillis()))
          .thenReply(replyTo)(_ => Confirmed)
      else
        Effect.reply(replyTo)(Rejected("No tickets available"))
    case GetReservations(userId, replyTo) =>
      Effect.reply(replyTo)(getUserReservations(userId))
    case BuyTickets(userId, replyTo) =>
      val ticketIds = tickets.collect {
        case ReservedTicket(ticketId, reservedBy, _, _) if reservedBy == userId => ticketId
      }
      Effect.persist(ReservationConfirmed(userId, ticketIds.toSeq)).thenReply(replyTo)(_ => Confirmed)
    case GetTickets(userId, replyTo) =>
      Effect.reply(replyTo)(getUserTickets(userId))
    case GetSoldTickets(replyTo) =>
      Effect.reply(replyTo)(getSoldTickets)
    case CreateEvent(eventId, _, _, _, replyTo, _) => Effect.reply(replyTo)(Rejected(s"Event $eventId is already created"))
    case UpdateEvent(maxVisitors, eventDate, address, replyTo) =>
      if (maxVisitors >= tickets.size)
        Effect
          .persist(EventUpdated(maxVisitors, eventDate, address))
          .thenReply(replyTo)(_ => Confirmed)
      else
        Effect.reply(replyTo)(Rejected(s"A number of tickets cannot be less than ${tickets.size}"))
    case _ => throw new IllegalStateException(s"unexpected command [$cmd]")
  }

  private def getUserReservations(userId: String) = {
    UserReservations(tickets.collect[ReservedTicket] {
      case ticket: ReservedTicket if ticket.userId == userId => ticket
    })
  }

  private def getUserTickets(userId: String) = {
    UserTickets(tickets.collect[SoldTicket] {
      case ticket: SoldTicket if ticket.userId == userId => ticket
    })
  }

  private def getSoldTickets: CommandReply = {
    UserTickets(tickets.collect[SoldTicket] {
      case ticket: SoldTicket => ticket
    })
  }

  def applyEvent(event: Event): TicketedEvent = event match {
    case TicketReserved(userId, ticketId, at) =>
      copy(tickets = tickets + ReservedTicket(
        ticketId, userId,
        price = Random.nextDouble(),
        validUntil = at + maxReservationTime))
    case ReservationConfirmed(_, ticketIds) =>
      val set = ticketIds.toSet
      copy(tickets = tickets.map({
        case ReservedTicket(ticketId, reservedBy, price, _) if set.contains(ticketId) =>
          SoldTicket(ticketId, reservedBy, price)
        case other => other
      }))
    case TicketReservationReleased(ticketId, _, _) => copy(tickets = tickets.filter {
      case ReservedTicket(reservedTicketId, _, _, _) if reservedTicketId == ticketId => false
    })
    case EventUpdated(maxVisitors, eventDate, address) =>
      copy(maxVisitors = maxVisitors, eventDate = eventDate, address = address)
    case _ => throw new IllegalStateException(s"Unexpected event [$event]")
  }

}

object TicketsSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[EventCreated],
    JsonSerializer[EventUpdated],
    JsonSerializer[TicketReserved],
    JsonSerializer[ReservationConfirmed],
    JsonSerializer[TicketReservationReleased],
  )
}
